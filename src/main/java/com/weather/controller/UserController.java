package com.weather.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.weather.exception.WeatherApplicationException;
import com.weather.model.User;
import com.weather.model.UserLoginHistory;
import com.weather.service.UserService;
@RestController
public class UserController {

	@Autowired
	UserService userService;

	@RequestMapping(value ="/addUser", method = RequestMethod.POST, headers = "Accept=application/json")
	public @ResponseBody Object createUser(@RequestBody User user) {
		try {
			userService.createUser(user);
		} catch (Exception e) {
			throw new WeatherApplicationException();
		}
		return user;
	}

	@RequestMapping(value ="/userRequestLogin/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody Object userLoginRequest(@PathVariable int id) {
		try {
			userService.generateLoginLink(id);
		} catch (Exception e) {
			throw new WeatherApplicationException();
		}
		Map<String, String> hMap = new HashMap<String, String>();
		hMap.put("message", "Check your email for the login link");
		return hMap;
	}

	@RequestMapping(value ="/login/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody Object userLogin(@PathVariable int id , @RequestParam String session) {
		try {
			User userToReturn = userService.getUserLoginHistory(id, session);
			if(userToReturn!=null){
				return userToReturn; 
			}
		} catch (Exception e) {
			throw new WeatherApplicationException();
		}
		Map<String,String> hMap = new HashMap<String, String>();
		hMap.put("message", "Sorry your link is now invalid");
		return hMap;
	}

}
