package com.weather.exception;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR)  
public class WeatherApplicationException extends RuntimeException {
	
	public WeatherApplicationException (){
		super("Internal Error");
	}
}
