package com.weather.dao;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.weather.model.User;
import com.weather.model.UserLoginHistory;
@Repository
public class UserDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public User createUser(User user) {
		Session session = this.sessionFactory.getCurrentSession();
		session.saveOrUpdate(user);
		return user;
	}

	public User getUser(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		User u = (User) session.get(User.class, id);
		return u;
	}

	public String saveLoginRequest(User u) {
		Session session = this.sessionFactory.getCurrentSession();
		UserLoginHistory ulh = new UserLoginHistory();
		String uuid = UUID.randomUUID().toString();
		ulh.setUser(u);
		ulh.setUuid(uuid);
		ulh.setCreatedDate(new Timestamp(System.currentTimeMillis()));
		ulh.setLoginStatus("F");
		session.save(ulh);
		return uuid;
	}

	public List<UserLoginHistory> getUserLoginHistory(int id, String uuid) {
		Session session = this.sessionFactory.getCurrentSession();
		User u = (User) session.get(User.class, id);
		Set<UserLoginHistory> ss = u.getUserLoginHistory();
		List<UserLoginHistory> s =  null;
		s = ss.
				stream().
				filter(ulh -> ulh.getUuid().equals(uuid)).
				collect(Collectors.toList());
		return s;
	}

}
