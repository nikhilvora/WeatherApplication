package com.weather.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
@Table(name="USER_DETAILS")
@JsonInclude(Include.NON_EMPTY)
@SequenceGenerator(name= "USER_ID_SEQ", sequenceName = "USER_ID_SEQ", initialValue=1, allocationSize = 1)
public class User{
	
	@Id
	@Column(name="USER_ID")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="USER_ID_SEQ")
	Integer userId;
	
	@Column(name="email")
	String email;	

	@Column(name="name")
	String name;	

	@Column(name="pincode")
	Integer pincode;
	
	@OneToMany(mappedBy="user")
	private Set<UserLoginHistory> userLoginHistory;
	
	public User() {
		super();
	}
	public User(int i, String email, Integer pincode, String name) {
		super();
		this.userId = i;
		this.email = email;
		this.name = name;
		this.pincode  =pincode;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Set<UserLoginHistory> getUserLoginHistory() {
		return userLoginHistory;
	}
	public void setUserLoginHistory(Set<UserLoginHistory> userLoginHistory) {
		this.userLoginHistory = userLoginHistory;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getPincode() {
		return pincode;
	}
	public void setPincode(Integer pincode) {
		this.pincode = pincode;
	}
}