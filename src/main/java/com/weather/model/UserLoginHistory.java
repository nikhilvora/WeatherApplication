package com.weather.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="USER_LOGIN_HISTORY")
@SequenceGenerator(name= "USER_LOGIN_HISTORY_SEQ", sequenceName = "USER_LOGIN_HISTORY_SEQ", initialValue=1, allocationSize = 1)
public class UserLoginHistory {

	@Id
	@Column(name="user_login_history_id")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="USER_LOGIN_HISTORY_SEQ")
	int userLoginHistoryId;
	
	@ManyToOne
	@JoinColumn(name = "USER_ID", nullable = false)
	private User user;

	@Column(name="uuid")
	private String uuid;
	
	@Column(name = "CREATED_DATE")
	private Timestamp createdDate;

	@Column(name="LOGIN_STATUS",columnDefinition = "char",length=1)
	private String loginStatus;

	public int getUserLoginHistoryId() {
		return userLoginHistoryId;
	}

	public void setUserLoginHistoryId(int userLoginHistoryId) {
		this.userLoginHistoryId = userLoginHistoryId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getLoginStatus() {
		return loginStatus;
	}

	public void setLoginStatus(String loginStatus) {
		this.loginStatus = loginStatus;
	}
	
}
