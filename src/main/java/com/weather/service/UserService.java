package com.weather.service;

import com.weather.dao.UserDAO;
import com.weather.model.User;
import com.weather.util.EmailUtil;
import com.weather.model.UserLoginHistory;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("userService")
public class UserService {

	@Autowired
	UserDAO userDao;
	
	@Transactional
	public User createUser(User user) {
		return userDao.createUser(user);
	}

	@Transactional
	public String generateLoginLink(int id) {
		User user = userDao.getUser(id);
		String uuidGenerated = userDao.saveLoginRequest(user);
		String loginLink = "http://localhost:8980/WeatherApp/login/" + id +"?session=" + uuidGenerated;
		EmailUtil.sendEmail("abc@gmail.com", "abc@123", user.getEmail(), loginLink );
		return loginLink;
	}

	@Transactional
	public User getUserLoginHistory(int id, String session) {
		List<UserLoginHistory> list = userDao.getUserLoginHistory(id, session);
		User userToReturn = null;
		for(UserLoginHistory ulh : list){
			ulh.getCreatedDate().getTime();
			if((System.currentTimeMillis() - ulh.getCreatedDate().getTime())/60000  < 15){
				userToReturn = ulh.getUser();
				userToReturn.setUserLoginHistory(null);
				userToReturn.setUserId(null);
				return userToReturn;
			}
		}
		return userToReturn;
	}
	
}
