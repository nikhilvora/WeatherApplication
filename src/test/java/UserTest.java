import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.util.*;
import org.springframework.transaction.annotation.Transactional;

import com.weather.controller.UserController;
import com.weather.model.User;
import com.weather.service.UserService;

import junit.framework.TestCase;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(locations = {
        "classpath:WEB-INF/spring-servlet.xml"})
public class UserTest extends TestCase {

	@Autowired
	private UserService userService;

	
	public UserTest() {
		super();
	}

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	
	@Test
	public void testCreateUser(){
		User user= new User();
		user.setEmail("a@b.com");
		userService.createUser(user);
		System.out.println("Created user id " + user.getUserId());
		assertNotNull(user.getUserId());
	}
	
	@Test
	public void testGenerateLoginLink(){
		String loginLink  = userService.generateLoginLink(3);
		System.out.println("Login Link " + loginLink);
		assertNotNull(loginLink);
		
	}
	
}
